# Quick memcached deployment

Project for quick memcached deployment with [Polemarch](https://github.com/vstconsulting/polemarch). 

Supported OS
------------
This project is able to deploy memcached on hosts with following OS:

* Ubuntu;
* Debian;
* RedHat;
* CentOS.

Project's playbook
------------------

* **deploy.yml**: This playbook deploys memcached on all hosts from
selected inventory.

Enjoy it!